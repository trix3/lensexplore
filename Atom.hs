{-# LANGUAGE TemplateHaskell #-}
import Control.Lens
import Control.Lens.TH

data Person = Person {_name :: String, _title :: String, _records :: Int} deriving Show

makeLenses ''Person

addRecord :: Person -> Person
addRecord = over (records) (+1)

data Company = Company {_employees :: [Person], _domain :: String} deriving Show

makeLenses ''Company

k = Person {_name = "k", _title = "writer", _records = 2}
m = Person {_name = "m", _title = "sales",  _records = 3}

qt = Company {_employees = [m, k], _domain = "Finance"}

--addRecord :: Person -> Person
companyAddRecord :: Company -> Company
companyAddRecord = over (employees . traverse . records) (+ 1)


viewDomain :: Company -> String 
viewDomain = view domain

setDomainTech :: Company -> Company
setDomainTech = set domain "Tech" 


